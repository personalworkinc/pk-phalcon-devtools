<?php
 //TODO somethin to do
$C = array(
    'personalwork' => array(
        // 配置專案名稱
        'projectname' => '@@name@@ Phalcon Framework',
        // 配置專案載入樣板檔使用前綴版型名稱
        'themename' => 'kingadmin',
        // 配置Personalwork預設頁面樣板檔路徑，起始路徑要以apps/views/為主配置相對路徑格式。
        'viewphtml' => '../../vendor/sanhuang/phalcon-personalwork/libs/Mvc/View/Phtmls/'
    ),
    'database' => array(
        'adapter'  => 'Pdo',
        'host'     => '127.0.0.1',
        'username' => 'sanslave',
        'password' => 'omge0954',
        'dbname'   => '@@name@@_phalcon',
        'charset'  => 'utf8',
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../apps/core/controllers/',
        'modelsDir'      => __DIR__ . '/../apps/models/',
        'formsDir'       => __DIR__ . '/../apps/forms/',
        'pluginsDir'     => __DIR__ . '/../apps/plugins/',
        'viewsDir'       => __DIR__ . '/../apps/views/',
        'baseUri'        => '/'
    ),
    'misc' => array(
        'headerStyle' => array(
            // bower:css
            // endbower
        ),
        'headerScript' => array(
            // bower:js
            // endbower
        ),
        'footerScript' => array(
            'js/frontend.js',
            'js/class-form.js'
        ),
    ),
    'mail' => array(
        'fromName'  => '@@name@@',
        'fromEmail' => 'san@personalwork.tw',
        'smtp'      => array(
                           'server' => "smtp.gmail.com",
                           'port'   => 465,
                           'security' => 'ssl',
                           'username' => 'san.personalwork@gmail.com',
                           'password' => 'ek4wu3j;3ru4'
                       )
    )
);

if( file_exists(__DIR__.'/application.json') ){
    $sysconf = new \Phalcon\Config\Adapter\Json( __DIR__.'/application.json' );
    $C['sysconfig'] = $sysconf->toArray();
}

return new \Phalcon\Config($C);