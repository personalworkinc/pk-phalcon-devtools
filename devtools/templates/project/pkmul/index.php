<?php
use Phalcon\Mvc\Application;

// define global const
define('PPS_APP_PROJECTNAME', '@@name@@');
define('PPS_APP_APPSPATH', __DIR__ . '/../apps');
// set session path
ini_set('session.save_path', PPS_APP_APPSPATH.'/../data/session' );

// templary for develop
error_reporting(E_ALL);
ini_set('display_errors', 'on');
$debug=new \Phalcon\Debug();
$debug->listen();

/**
 * 讀取README.md抓取裡面最後一筆版本定義
 */
if( $fp=fopen(getcwd().'/../README.md','r') ){
    while ( ($line = fgets($fp)) !== false ) {
        if( preg_match("/^v\s([^\(]+)/",$line,$m) ){
            define('VERSION', $m[1]);
            break;
        }
    }
    if( !defined('VERSION') ){ define('VERSION','0.5.0'); }
}

/**
 * 預設使用mbstring來判斷字元長度
 * @see https://github.com/phalcon/cphalcon/issues/971
 */
if( function_exists('mb_internal_encoding') )
    mb_internal_encoding("utf-8");

/**
 * Read the configuration
 */
$config = @@configLoader@@;

require __DIR__ . '/../config/services.php';

/**
 * Handle the request
 */
$application = new Application($di);
/**
 * Include modules
 * Register application modules
 */
$application->registerModules(array(
    'core' => array(
        'className' => '@@namespace@@\Core\Bootstrap',
        'path' => __DIR__ . '/../apps/core/Bootstrap.php'
    ),
    'phase' => array(
        'className' => '@@namespace@@\Phase\Bootstrap',
        'path' => __DIR__ . '/../apps/phase/Bootstrap.php'
    )
));

/**
 * Include routes
 */
require PPS_APP_APPSPATH . '/../config/routes.php';

echo $application->handle()->getContent();
