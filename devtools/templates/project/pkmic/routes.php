<?php

$router = $di->get("router");
/**
 * 配置預設route指向初始化Action
 * */
$router->add('/', array(
    'namespace' => ucfirst('@@name@@').'\Core\Controllers',
    'module' => 'core',
    'controller' => 'default',
    'action' => 'index'
));

foreach ($application->getModules() as $key => $module) {
    $namespace = str_replace('Module','Controllers', $module["className"]);
    $router->add('/'.$key.'/:params', array(
        'namespace' => $namespace,
        'module' => $key,
        'controller' => 'default',
        'action' => 'index',
        'params' => 1
    ))->setName($key);
    $router->add('/'.$key.'/:controller/:params', array(
        'namespace' => $namespace,
        'module' => $key,
        'controller' => 1,
        'action' => 'index',
        'params' => 2
    ));
    $router->add('/'.$key.'/:controller/:action/:params', array(
        'namespace' => $namespace,
        'module' => $key,
        'controller' => 1,
        'action' => 2,
        'params' => 3
    ));
}

$router->notFound(array(
    "module" => "core",
    "controller" => "default",
    "action" => "route404"
));
$router->handle();
return $router;