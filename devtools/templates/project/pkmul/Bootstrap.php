<?php
namespace @@namespace@@\@@modname@@;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Bootstrap implements ModuleDefinitionInterface
{
    /**
     * Registers the module auto-loader
     */
    public function registerAutoloaders(\Phalcon\DiInterface $di = null)
    {
        if( $di->has('autoloader') ){
            $loader = $di->get('autoloader');
        }else{
            $loader = new Loader();
        }

        $namespace = $loader->getNamespaces();
        //module's controller / form /
        $loader->registerNamespaces(array_merge($namespace, array(
            '@@namespace@@\@@modname@@\Controllers'  => __DIR__ .'/controllers/',
            '@@namespace@@\@@modname@@\Forms'        => __DIR__ .'/forms/',
            '@@namespace@@\@@modname@@\Plugins'      => __DIR__ .'/plugins/',
        )));

        $loader->register();
    }


    /**
     * Registers the module-only services
     *
     * @param Phalcon\DI $di
     */
    public function registerServices(\Phalcon\DiInterface $di = null)
    {
        if( $di->has('dispatcher') ){
            $dispatcher = $di->get('dispatcher');
        }else{
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
        }
        $dispatcher->setDefaultNamespace('\@@namespace@@\@@modname@@\Controllers');

        /**
         * Setting up the view component
         * use in Phalcon v3.0.x
         * 處理方式：取得$di['view']重新設定路徑，目前官方版本有個bug在設定Multiple viewsDir後導致view render時每個layout、partial都會重複render！
         */
        $path = $di->get('view')->getViewsDir();
        $path[] = __DIR__ . '/views/';
        $di->get('view')->setViewsDir( $path );

    }
}
