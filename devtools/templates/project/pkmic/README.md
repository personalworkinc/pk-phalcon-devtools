# @@name@@ 專案說明文件

---
## 更版資訊
> 首號0為開發/測試階段，1為正式上線後 [0].6dev
> 次號為新功能完成或更新時跳號紀錄 0.[6]dev
> 次子號單數為相對發佈版(功能為調整、bug修正)、雙數為臨時釋出紀錄版(上版後補充修正時) 0.6.[1]dev
> 附加標記：dev開發 | alpha前期測試 | beta測試 | release發布 | stable穩定釋出


## 結構注意
- apps/controllers/DefaultController.php檔案內必須配置
```php
    public function initialize() {
        // 呼叫Phalcon建構子
        parent::initialize();
    }
```

- 專案建立後必須在終端機初始化指令順序
> 針對composer.json
> $compoers install

> 針對npm安裝grunt與wiredep外掛
(將grunt / grunt-wiredep 加到node全域套件內。)
> $npm install grunt-wiredep --save-dev

(同上，加入到node全域套件內。)
> $npm install grunt-contrib-watch --save-dev
> 針對bower.json

(初始化安裝預設套件於.bowerrc指定目錄public/js)
> $bower install

(或者用於開發中專案)
> $bower install [packagename] --save

(透過grunt與grunt-wiredep配置)
> $grunt wiredep

---
## 任務進度
