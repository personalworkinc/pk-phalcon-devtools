<?php

$C = array(
    'personalwork' => array(
        // 配置專案名稱
        'projectname' => 'PPS預設模組系統',
        // 配置專案載入樣板檔使用前綴版型名稱
        'themename' => 'kingadmin',
        // 配置Personalwork預設頁面樣板檔路徑，起始路徑要以apps/views/為主配置相對路徑格式。
        'viewphtml' => '../../vendor/sanhuang/phalcon-personalwork/libs/Mvc/View/Phtmls/'
    ),
    'database' => array(
        'adapter'   => 'Mysql',
        'host'      => '127.0.0.1',
        'username'  => 'sanslave',
        'password'  => 'omge0954',
        'dbname'    => 'devel_@@name@@',
        'charset'   => 'utf8'
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../apps/frontend/controllers/',
        'modelsDir'      => __DIR__ . '/../apps/models/',
        'pluginDir'       => __DIR__ . '/../apps/plugins/',
        'baseUri'        => '/'
    ),
    'misc' => array(
        'headerCss' => array(
            'misc/css/main.css',
            'misc/css/layouts-application.css'
        ),
        'headerScript' => array(
            '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'
        ),
        'footerScript' => array(
            'misc/js/application.js',
            'misc/js/form-extend.js'
        ),
    ),
    'mail' => array(
        'fromName'  => 'PPS',
        'fromEmail' => 'san@personalwork.tw',
        'smtp'      => array(
                           'server' => "smtp.gmail.com",
                           'port'   => 465,
                           'security' => 'ssl',
                           'username' => 'san.personalwork@gmail.com',
                           'password' => 'ek4wu3j;3ru4'
                       )
    )
);


if( file_exists(__DIR__.'/application.json') ){
    $sysconf = new \Phalcon\Config\Adapter\Json( __DIR__.'/application.json' );
    $C['sysconfig'] = $sysconf->toArray();
}

return new \Phalcon\Config($C);
