<?php
$router = new \Phalcon\Mvc\Router(FALSE);
$router->removeExtraSlashes(true);

/**
 * Global routes
 */
$router->add('/:module/:controller/:action(/:params)?', array(
    'module' => 1,
    'controller' => 2,
    'action' => 3,
    'param' => 4
));
/**
 * define default route
 * */
$router->add("/(index|dashboard)?", array(
    'namespace' => ucfirst(PPS_APP_PROJECTNAME) . '\Phase\Controllers\\',
    'module'    => 'phase',
    'controller'=> 'default',
    'action'    => 'index'
));

$router->handle();
//Get the matched route
// $route = $router->getMatchedRoute();
// echo $router->getModuleName()."<BR/>";
// echo $router->getControllerName()."<BR/>";
// echo $router->getActionName()."<BR/>";
// var_dump($router->getParams());
// var_dump($route);
// die();
return $router;


//These routes simulate real URIs
$testRoutes = array(
    '/',
    '/index',
    '/default',
    '/index/test/a/b/c/d',
    '/edit/basepath/see/doc/'
);
//Testing each route
foreach ($testRoutes as $testRoute) {
    //Handle the route
    $router->handle($testRoute);
    echo 'Testing ', $testRoute, '<br>';
    //Check if some route was matched
    if ($router->wasMatched()) {
        echo 'Module: ', $router->getModuleName(), '<br>';
        echo 'Controller: ', $router->getControllerName(), '<br>';
        echo 'Action: ', $router->getActionName(), '<br>';
        echo 'Params: ', implode(", ", $router->getParams()), '<br>';
    } else {
        echo 'The route wasn\'t matched by any route<br>';
    }
    echo '<br>';
}
