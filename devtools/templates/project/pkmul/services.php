<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Mvc\Router;
use Phalcon\Loader;
use Phalcon\Text;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Cache\Frontend\Data as CacheFrontData;
use Phalcon\Cache\Backend\File as CacheBackFile;

/**
 * The FactoryDefault Dependency Injector automatically registers the right services to provide a full stack framework
 */
$di = new FactoryDefault();

/**
 * Registering a global config
 * 注意！在Phalcon3.0.x時由於$di['config']並非預設服務，故在尚未呼叫存取之前物並並未產生
 */
$di->set('config', require __DIR__.'/config.php');

/**
 * Registering a router
 * @see https://github.com/phalcon/album-o-rama/blob/master/public/index.php
 */
$di->set('router', require __DIR__.'/routes.php');

/**
 * The URL component is used to generate all kinds of URLs in the application
 * use in Phalcon v3.0.x
 * 處理方式：直接可在匿名函式內使用$this代表$DI
 */
// url
$di->setShared('url', function () {
    $config = $this->getService('config')->resolve();
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});
// view
$di->setShared('view', function () use ($di) {
    $config = $this->getService('config')->resolve();
    $view = new View();
    $view->setViewsDir( [$config->application->viewsDir] )
         ->setLayoutsDir( $config->application->layoutsDir )
         ->setLayout( $config->application->layoutName );
    return $view;
});


$di['navigation'] = function() {
    $nav = new \Phalcon\Config\Adapter\Json( PPS_APP_APPSPATH . '/../config/navigation.json' );
    return new \Personalwork\Navigation( $nav->navigation );
};

$di['db'] = function () use ($di) {
    return new DbAdapter(array(
        "host"      => $di->get('config')->database->host,
        "username"  => $di->get('config')->database->username,
        "password"  => $di->get('config')->database->password,
        "dbname"    => $di->get('config')->database->dbname,
        'charset'   => $di->get('config')->database->charset,
        "options"   => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
};

$di->set('logger', function () use ($di) {

    $logger = new \Personalwork\Logger\Adapter\Database(array(
                    'db'    => $di->get('db'),
                    'table' => "logger",
                    'name'  => 'info',
                    'router'=> $di->get('router'),
                    'request'=> $di->get('request'),
                    'session'=> $di->get('session'),
                  ));

    return $logger;
});

$di->set('autoloader', function() {
    $Loader = new Loader();

    $Loader->registerNamespaces(array(
        ucfirst(PPS_APP_PROJECTNAME) . '\Models'                  => __DIR__ . '/../apps/models/',
        ucfirst(PPS_APP_PROJECTNAME) . '\Plugins'                 => __DIR__ . '/../apps/plugins/',
    ));

    if (!file_exists(PPS_APP_APPSPATH . '/../vendor/autoload.php')) {
        echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
        exit(1);
    }
    require_once PPS_APP_APPSPATH . '/../vendor/autoload.php';

    return $Loader;
});


/**
 * Set the models cache service
 * 後續只要直接在ORM物件內
 * 配置array('cache'=>array('key'=>[keyname],'lifetime'=>[sec]))
 * 即可使用
 * */
$di->set('modelsCache', function () {
    // Cache data for one day by default
    $frontCache = new CacheFrontData(
        array(
            "lifetime" => 86400
        )
    );

    $path=PPS_APP_APPSPATH."/../public/data/cache";
    if( !is_dir($path) ){
        @mkdir($path, 0777);
        @chmod($path, 0777);
    }
    $cachepath = realpath($path).'/';
    // Memcached connection settings
    $cache = new CacheBackFile(
        $frontCache,
        array(
            "cacheDir" => $cachepath
        )
    );

    return $cache;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Starts the session the first time some component requests the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set( 'flashSession', function(){
    return new \Phalcon\Flash\Session(array(
        'error' => 'alert alert-danger text-center',
        'success' => 'alert alert-success text-center',
        'notice' => 'alert alert-info text-center',
        'warning' => 'alert alert-warning text-center',
    ));
});

/**
* Set the default namespace for dispatcher
*/
$di->setShared('dispatcher', function() use ($di) {
    //Camelize actions
    $eventsManager = $di->getShared('eventsManager');
    $dispatcher = new Phalcon\Mvc\Dispatcher();
    $eventsManager->attach("dispatch:beforeDispatchLoop", function($event, $dispatcher) {
        $dispatcher->setActionName(Text::camelize($dispatcher->getActionName()));
    });

    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});
