<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */
use Phalcon\Loader;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\View as View;
use Phalcon\Text;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * The FactoryDefault Dependency Injector automatically registers the right services to provide a full stack framework
 */
$di = new FactoryDefault();

/**
 * Registering a global config
 */
$di->set('config', require __DIR__.'/config.php');

if( is_file(PPS_APP_APPSPATH . '/../config/navigation.json') ){
$di['navigation'] = function() {
    $nav = new \Phalcon\Config\Adapter\Json( PPS_APP_APPSPATH . '/../config/navigation.json' );
    var_dump($nav->navigation);die();
    return new \Personalwork\Navigation( $nav->navigation );
};
}

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di['db'] = function () use ($di) {
    return new DbAdapter(array(
        "host"      => $di->get('config')->database->host,
        "username"  => $di->get('config')->database->username,
        "password"  => $di->get('config')->database->password,
        "dbname"    => $di->get('config')->database->dbname,
        'charset'   => $di->get('config')->database->charset,
        "options"   => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
};

$di->set('logger', function () use ($di) {

    $logger = new \Personalwork\Logger\Adapter\Database(array(
                    'db'    => $di->get('db'),
                    'table' => "logger",
                    'name'  => 'info',
                    'router'=> $di->get('router'),
                    'request'=> $di->get('request'),
                    'session'=> $di->get('session'),
                  ));

    return $logger;
});

/**
 * The URL component is used to generate all kinds of URLs in the application
 */
$di->setShared('url', function () use($di) {
    $url = new UrlResolver();
    $url->setBaseUri($di['config']->application->baseUri);

    return $url;
});

/**
 * Register composer vendor in autoloader
 **/
$di->set('autoloader', function() {
    $Loader = new Loader();

    $Loader->registerNamespaces(array(
        ucfirst(PPS_APP_PROJECTNAME) . '\Models'                  => PPS_APP_APPSPATH . '/models/',
        ucfirst(PPS_APP_PROJECTNAME) . '\Plugins'                 => PPS_APP_APPSPATH . '/plugins/',
    ));

    if (!file_exists(PPS_APP_APPSPATH . '/../vendor/autoload.php')) {
        echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
        exit(1);
    }
    require_once PPS_APP_APPSPATH . '/../vendor/autoload.php';

    return $Loader;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($di) {
    $view = new View();
    $view->setViewsDir($di->get('config')->application->viewsDir);
    $view->registerEngines(array(
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Starts the session the first time some component requests the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set( 'flashSession', function(){
    return new \Phalcon\Flash\Session(array(
        'error' => 'alert alert-danger text-center',
        'success' => 'alert alert-success text-center',
        'notice' => 'alert alert-info text-center',
        'warning' => 'alert alert-warning text-center',
    ));
});

/**
 * 實作類似Zend-Framework aaBa => aa-ba
 * @see http://docs.phalconphp.com/en/latest/reference/dispatching.html#camelize-action-names
 *
 * @todo 附加身份驗證檢查
 */
$di['dispatcher'] = function() use ($di) {
    $dispatcher = new Phalcon\Mvc\Dispatcher();

    $eventsManager = $di->getShared('eventsManager');
    //Camelize actions
    $eventsManager->attach("dispatch:beforeDispatchLoop", function($event, $dispatcher) {
        $dispatcher->setActionName(Text::camelize($dispatcher->getActionName()));
    });
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
};