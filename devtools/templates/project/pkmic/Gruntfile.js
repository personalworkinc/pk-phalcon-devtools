/**
 * Gruntfile use grunt-wiredep for autoinsert javascript / css file to config.php
 *
 * @see https://www.npmjs.com/package/wiredep 語法說明
 *
 * */
module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-wiredep');

  grunt.initConfig({
    wiredep: {
      // task 只是識別碼，可以為其他字串例如:task1、task2...
      task: {
        src: ['config/config.php'],
        // 相對於src目錄算ignorePath路徑
        ignorePath: '../public/',
        // 可以指定多種fileTypes:html\phtml\ini\yaml等..
        fileTypes: {
          php: {
            block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
            detect: {
              js: /<script.*src=['"]([^'"]+)/gi,
              css: /<link.*href=['"]([^'"]+)/gi
            },
            replace: {
              js: '\'{{filePath}}\',',
              css: '\'{{filePath}}\','
            }
          }
        }
      }
    }
  });

  grunt.registerTask('default', ['wiredep']);
};