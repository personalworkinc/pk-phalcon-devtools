<?php
//TODO 測試抓取
use Phalcon\Mvc\Application;

/**
 * templary for develop
 *
    error_reporting(E_ALL);
    ini_set('display_errors', 'on');
    $debug=new \Phalcon\Debug();
    $debug->listen();
 */

define('PPS_APP_PROJECTNAME', '@@name@@');
define('PPS_APP_APPSPATH', realpath('../apps'));
/**
 * 讀取README.md抓取裡面最後一筆版本定義
 */
if( $fp=fopen(getcwd().'/../README.md','r') ){
    while ( ($line = fgets($fp)) !== false ) {
        if( preg_match("/^v\s([^\(]+)/",$line,$m) ){
            define('PPS_APP_VERSION', $m[1]);
            break;
        }
    }
    if( !defined('PPS_APP_VERSION') ){ define('PPS_APP_VERSION','0.5.0'); }
}


try {

    /**
     * Read the configuration
     */
    $config = @@configLoader@@;

    /**
     * Include services
     */
    require PPS_APP_APPSPATH . '/../config/services.php';

    /**
     * Handle the request
     */
    $application = new Application($di);

    /**
     * Register application modules
     */
    $application->registerModules(array(
        'core' => array(
            'className' => ucfirst('@@name@@').'\Core\Module',
            'path' => PPS_APP_APPSPATH . '/core/Bootstrap.php'
        )
    ));

    /**
     * Include routes
     */
    require PPS_APP_APPSPATH . '/../config/routes.php';

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
