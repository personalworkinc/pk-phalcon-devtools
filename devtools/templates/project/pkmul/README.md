# @@name@@ 專案說明文件

>
-----------------------
## 版本
v 0.5.0([date])
>

## 結構注意
- apps/core/controllers/DefaultController.php檔案內可配置
```php
    public function initialize() {
        // 呼叫Phalcon建構子
        parent::initialize();
    }
```

- 配置一個新的模組[content]作法
    1. 複製整個phase目錄改名為新模組放在apps下
    2. 在新模組下的Bootstrap.php內將所有"Phase"取代為"Content"
    3. 修改/public/index.php內於 $application->registerModules 片段附加新模組

```php
$application->registerModules(array(
    'core' => array(
        'className' => 'House\Core\Bootstrap',
        'path' => __DIR__ . '/../apps/core/Bootstrap.php'
    ),
    //附加的新模組
    'content' => array(
        'className' => 'House\Content\Bootstrap',
        'path' => __DIR__ . '/../apps/content/Bootstrap.php'
    ),
    'phase' => array(
        'className' => 'House\Phase\Bootstrap',
        'path' => __DIR__ . '/../apps/phase/Bootstrap.php'
    )
));
```
-----------------------
## 任務進度
