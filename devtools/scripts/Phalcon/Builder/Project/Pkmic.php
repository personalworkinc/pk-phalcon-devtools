<?php

/*
  +------------------------------------------------------------------------+
  | Phalcon Developer Tools                                                |
  +------------------------------------------------------------------------+
  | Copyright (c) 2011-2016 Phalcon Team (http://www.phalconphp.com)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file docs/LICENSE.txt.                        |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconphp.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Authors: Andres Gutierrez <andres@phalconphp.com>                      |
  |          Eduar Carvajal <eduar@phalconphp.com>                         |
  +------------------------------------------------------------------------+
*/

namespace Phalcon\Builder\Project;

use Phalcon\Builder\Controller as ControllerBuilder;
use Phalcon\Web\Tools;

/**
 * Multi-Module
 *
 * Builder to create Multi-Module application skeletons
 *
 * @package Phalcon\Builder\Project
 */
class Pkmic extends ProjectBuilder
{
    /**
     * Project directories
     * @var array
     */
    protected $projectDirectories = array(
        'apps/',
        'apps/core',
        'apps/core/controllers',
        'apps/models',
        'apps/plugins',
        'apps/views',
        'apps/views/layouts',
        'apps/views/default',
        'config/',
        'data/cache',
        'data/logs',
        'data/session',
        'public',
        'public/misc',
        'public/misc/img',
        'public/misc/css',
        'public/misc/js',
        'public/bower_component',
        'public/uploads',
        '.gitignore',
        '.phalcon'
    );



    /**
     * 2017-04-20
     *
     * 新配置參數 --theme=
     * 根據/templates/theme/目錄內指定版型複製部分檔案到新專案內
     * ex: theme=kingadmin
     *
     * @return $this
     */
    private function copyThemeDirectory()
    {
        $toppath = $this->options->get('themePath');
        // 複製說明目錄內容

        // 指定html檔案放置到views 或 layouts內

        // 取得css與圖檔複製

        // $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }

    /**
     * Create ControllerBase
     *
     * @return $this
     */
    private function createControllerBase()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmic/ControllerBase.php';
        $putFile = $this->options->get('projectPath') . 'apps/core/controllers/ControllerBase.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }

    /**
     * Create phaseController file
     *
     * @return $this
     */
    private function createControllerFile()
    {
        $namespace = $this->options->get('name');
        if (strtolower(trim($namespace)) == 'default') {
            $namespace = 'MyDefault';
        }

        $builder = new ControllerBuilder(array(
            'name'           => 'default',
            'controllersDir' => $this->options->get('projectPath') . 'apps/core/controllers/',
            'namespace'      => ucfirst($namespace) . '\Core\Controllers',
            'baseClass'      => '\Personalwork\Mvc\Controller\Base\Application'
        ));

        $builder->build();

        return $this;
    }

    /**
     * Create .htaccess files by default of application
     *
     * @return $this
     */
    private function createHtaccessFiles()
    {
        if (file_exists($this->options->get('projectPath') . '.htaccess') == false) {
            $code = '<IfModule mod_rewrite.c>' . PHP_EOL .
                "\t" . 'RewriteEngine on' . PHP_EOL .
                "\t" . 'RewriteRule  ^$ public/    [L]' . PHP_EOL .
                "\t" . 'RewriteRule  (.*) public/$1 [L]' . PHP_EOL .
                '</IfModule>';
            file_put_contents($this->options->get('projectPath') . '.htaccess', $code);
        }

        if (file_exists($this->options->get('projectPath') . 'public/.htaccess') == false) {
            file_put_contents(
                $this->options->get('projectPath') . 'public/.htaccess',
                file_get_contents($this->options->get('templatePath') . '/project/pkmic/htaccess')
            );
        }

        if (file_exists($this->options->get('projectPath') . 'index.html') == false) {
            $code = '<html><body><h1>Mod-Rewrite is not enabled</h1><p>Please enable rewrite module on your web server to continue</body></html>';
            file_put_contents($this->options->get('projectPath') . 'index.html', $code);
        }

        return $this;
    }

    /**
     * Create view and layout files by default
     *
     * @TODO : 加入kingadmin-v1.5預設版型所需檔案，並可透過參數判斷安裝
     * @return $this
     */
    private function createViewFiles()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmic/views/index.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/views/index.phtml';
        $this->generateFile($getFile, $putFile);

        $getFile = $this->options->get('templatePath') . '/project/pkmic/views/layouts-default.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/views/layouts/default.phtml';
        $this->generateFile($getFile, $putFile);

        $getFile = $this->options->get('templatePath') . '/project/pkmic/views/default/index.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/views/default/index.phtml';
        $this->generateFile($getFile, $putFile);
        // @TODO : 需要自行增加route404Actionblock
        $getFile = $this->options->get('templatePath') . '/project/pkmic/views/default/route404.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/views/default/route404.phtml';
        $this->generateFile($getFile, $putFile);

        return $this;
    }

    /**
     * Creates the configuration
     *
     * @return $this
     */
    private function createConfig()
    {
        $type = $this->options->contains('useConfigIni') ? 'ini' : 'php';

        $getFile = $this->options->get('templatePath') . '/project/pkmic/config.' . $type;
        $putFile = $this->options->get('projectPath') . 'config/config.' . $type;
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }

    /**
     * Create Module
     *
     * @return $this
     */
    private function createModule()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmic/Module.php';
        $putFile = $this->options->get('projectPath') . 'apps/core/Bootstrap.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }

    /**
     * Create Bootstrap file by default of application
     *
     * @return $this
     */
    private function createBootstrapFile()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmic/index.php';
        $putFile = $this->options->get('projectPath') . 'public/index.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        $getFile = $this->options->get('templatePath') . '/project/pkmic/services.php';
        $putFile = $this->options->get('projectPath') . 'config/services.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        $getFile = $this->options->get('templatePath') . '/project/pkmic/routes.php';
        $putFile = $this->options->get('projectPath') . 'config/routes.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }

    /**
     * Creates the composer.json \ bower \ README.md \ .gitignore
     *
     * @return $this
     */
    private function createPackageFile()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmic/README.md';
        $putFile = $this->options->get('projectPath') . 'README.md';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        $getFile = $this->options->get('templatePath') . '/project/pkmic/composer.json';
        $putFile = $this->options->get('projectPath') . 'composer.json';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        // bower with grunt / grunt-wiredep
        $getFile = $this->options->get('templatePath') . '/project/pkmic/bower.json';
        $putFile = $this->options->get('projectPath') . 'bower.json';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));
        $getFile = $this->options->get('templatePath') . '/project/pkmic/.bowerrc';
        $putFile = $this->options->get('projectPath') . '.bowerrc';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));
        $getFile = $this->options->get('templatePath') . '/project/pkmic/Gruntfile.js';
        $putFile = $this->options->get('projectPath') . 'Gruntfile.js';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        // .gitignore
        $getFile = $this->options->get('templatePath') . '/project/pkmic/gitignore';
        $putFile = $this->options->get('projectPath') . '.gitignore';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }

    // /**
    //  * Create .htrouter.php file
    //  * @deprecated
    //  * @return $this
    //  */
    // private function createHtrouterFile()
    // {
    //     $getFile = $this->options->get('templatePath') . '/project/pkmic/.htrouter.php';
    //     $putFile = $this->options->get('projectPath') . '.htrouter.php';
    //     $this->generateFile($getFile, $putFile);

    //     return $this;
    // }

    /**
     * Build project
     *
     * @return bool
     */
    public function build()
    {
        $this
            ->buildDirectories()
            ->getVariableValues()
            ->createConfig()
            ->createBootstrapFile()
            ->createHtaccessFiles()
            // ->createControllerBase()
            ->createModule()
            ->createViewFiles()
            ->createControllerFile()
            ->createPackageFile();
            // ->createHtrouterFile();

        $this->options->contains('enableWebTools') && Tools::install($this->options->get('projectPath'));

        return true;
    }
}
