<?php
/**
 * @@controllername@@Controller
 *
 * */

namespace @@namespace@@\@@modname@@\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

class @@controllername@@Controller extends ControllerBase
{
    public function initialize() {
        parent::initialize();
    }


    /**
     *
     * */
    public function indexAction()
    {
    }
}
