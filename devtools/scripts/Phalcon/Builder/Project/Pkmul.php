<?php

/*
  +------------------------------------------------------------------------+
  | Phalcon Developer Tools                                                |
  +------------------------------------------------------------------------+
  | Copyright (c) 2011-2016 Phalcon Team (http://www.phalconphp.com)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file docs/LICENSE.txt.                        |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconphp.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Authors: Andres Gutierrez <andres@phalconphp.com>                      |
  |          Eduar Carvajal <eduar@phalconphp.com>                         |
  +------------------------------------------------------------------------+
*/

namespace Phalcon\Builder\Project;

use Phalcon\Builder\Controller as ControllerBuilder;
use Phalcon\Web\Tools;

/**
 * Multi-Module
 *
 * Builder to create Multi-Module application skeletons
 *
 * @package Phalcon\Builder\Project
 */
class Pkmul extends ProjectBuilder
{
    /**
     * Project directories
     * @var array
     */
    protected $projectDirectories = array(
        'apps/',
        'apps/core',
        'apps/core/controllers',
        'apps/core/forms',
        'apps/core/plugins',
        'apps/phase',
        'apps/phase/controllers',
        'apps/layouts',
        'apps/models',
        'apps/views',
        'apps/views/phase',
        'config/',
        'data',
        'data/cache',
        'data/logs',
        'data/session',
        'public',
        'public/misc/img',
        'public/misc/css',
        'public/misc/js',
        'public/uploads',
        '.phalcon'
    );

    /**
     * Create indexController file
     *
     * @return $this
     */
    private function createControllerFile()
    {
        // core
        $getFile = $this->options->get('templatePath') . '/project/pkmul/ControllerBase.php';
        $putFile = $this->options->get('projectPath') . 'apps/core/controllers/SystemController.php';
        $this->generateFileWithModname($getFile, $putFile, 'Core', 'System');

        // phase
        $getFile = $this->options->get('templatePath') . '/project/pkmul/ControllerBase.php';
        $putFile = $this->options->get('projectPath') . 'apps/phase/controllers/DefaultController.php';
        $this->generateFileWithModname($getFile, $putFile, 'Phase', 'Default');


        return $this;
    }

    /**
     * Create .htaccess files by default of application
     *
     * @return $this
     */
    private function createHtaccessFiles()
    {
        if (file_exists($this->options->get('projectPath') . '.htaccess') == false) {
            $code = '<IfModule mod_rewrite.c>' . PHP_EOL .
                "\t" . 'RewriteEngine on' . PHP_EOL .
                "\t" . 'RewriteRule  ^$ public/    [L]' . PHP_EOL .
                "\t" . 'RewriteRule  (.*) public/$1 [L]' . PHP_EOL .
                '</IfModule>';
            file_put_contents($this->options->get('projectPath') . '.htaccess', $code);
        }

        if (file_exists($this->options->get('projectPath') . 'public/.htaccess') == false) {
            file_put_contents(
                $this->options->get('projectPath') . 'public/.htaccess',
                file_get_contents($this->options->get('templatePath') . '/project/pkmul/htaccess')
            );
        }

        if (file_exists($this->options->get('projectPath') . 'index.html') == false) {
            $code = '<html><body><h1>Mod-Rewrite is not enabled</h1><p>Please enable rewrite module on your web server to continue</body></html>';
            file_put_contents($this->options->get('projectPath') . 'index.html', $code);
        }

        return $this;
    }

    /**
     * Create view files by default
     *
     * @return $this
     */
    private function createViewFiles()
    {
        // index.phtml
        $getFile = $this->options->get('templatePath') . '/project/pkmul/views/index.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/views/index.phtml';
        $this->generateFile($getFile, $putFile);

        // layouts
        $getFile = $this->options->get('templatePath') . '/project/pkmul/layouts/kingadmin.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/layouts/kingadmin.phtml';
        $this->generateFile($getFile, $putFile);

        $getFile = $this->options->get('templatePath') . '/project/pkmul/layouts/page-default.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/layouts/page-default.phtml';
        $this->generateFile($getFile, $putFile);

        $getFile = $this->options->get('templatePath') . '/project/pkmul/layouts/page-print.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/layouts/page-print.phtml';
        $this->generateFile($getFile, $putFile);

        // phase/
        $getFile = $this->options->get('templatePath') . '/project/pkmul/views/phase/index.phtml';
        $putFile = $this->options->get('projectPath') . 'apps/views/phase/index.phtml';
        $this->generateFile($getFile, $putFile);

        return $this;
    }

    /**
     * Creates the configuration
     *
     * @return $this
     */
    private function createConfigFiles()
    {
        // $type = $this->options->contains('useConfigIni') ? 'ini' : 'php';

      // config.php
      $getFile = $this->options->get('templatePath') . '/project/pkmul/config.php';
      $putFile = $this->options->get('projectPath') . 'config/config.php';
      $this->generateFile($getFile, $putFile, $this->options->get('name'));

      // navigation.json
      $getFile = $this->options->get('templatePath') . '/project/pkmul/navigation.json';
      $putFile = $this->options->get('projectPath') . 'config/navigation.json';
      $this->generateFile($getFile, $putFile, $this->options->get('name'));

      $getFile = $this->options->get('templatePath') . '/project/pkmul/services.php';
      $putFile = $this->options->get('projectPath') . 'config/services.php';
      $this->generateFile($getFile, $putFile, $this->options->get('name'));

      $getFile = $this->options->get('templatePath') . '/project/pkmul/routes.php';
      $putFile = $this->options->get('projectPath') . 'config/routes.php';
      $this->generateFile($getFile, $putFile, $this->options->get('name'));

      return $this;
    }

    /**
     * ControllerBase
     * @deprecated 直接修改預設Controller內載入 \Personalwork\Mvc\Controller\Base\Application
     * @return $this
    private function createControllerBase()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmul/ControllerBase.php';
        $putFile = $this->options->get('projectPath') . 'apps/phase/controllers/ControllerBase.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }
     */

    /**
     * Create Module
     *
     * @return $this
     */
    private function createBootstrap()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmul/Bootstrap.php';

        $putFile = $this->options->get('projectPath') . 'apps/core/Bootstrap.php';
        $this->generateFileWithModname($getFile, $putFile, 'Core');

        $putFile = $this->options->get('projectPath') . 'apps/phase/Bootstrap.php';
        $this->generateFileWithModname($getFile, $putFile, 'Phase');

        return $this;
    }

    /**
     * Create Bootstrap file by default of application
     *
     * @return $this
     */
    private function createPublicFiles()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmul/index.php';
        $putFile = $this->options->get('projectPath') . 'public/index.php';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        // css
        $getFile = $this->options->get('templatePath') . '/project/pkmul/misc/css/layouts-application.css';
        $putFile = $this->options->get('projectPath') . 'public/misc/css/layouts-application.css';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        $getFile = $this->options->get('templatePath') . '/project/pkmul/misc/css/layouts-print.css';
        $putFile = $this->options->get('projectPath') . 'public/misc/css/layouts-print.css';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        $getFile = $this->options->get('templatePath') . '/project/pkmul/misc/css/main.css';
        $putFile = $this->options->get('projectPath') . 'public/misc/css/main.css';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        // js
        $getFile = $this->options->get('templatePath') . '/project/pkmul/misc/js/application.js';
        $putFile = $this->options->get('projectPath') . 'public/misc/js/application.js';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        $getFile = $this->options->get('templatePath') . '/project/pkmul/misc/js/form-extend.js';
        $putFile = $this->options->get('projectPath') . 'public/misc/js/form-extend.js';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));


        return $this;
    }


    /**
     * Creates the composer.json \ bower \ README.md
     *
     * @return $this
     */
    private function createPackageFile()
    {
        // README.md
        $getFile = $this->options->get('templatePath') . '/project/pkmul/README.md';
        $putFile = $this->options->get('projectPath') . 'README.md';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));
        // task.todo
        $getFile = $this->options->get('templatePath') . '/project/pkmul/task.todo';
        $putFile = $this->options->get('projectPath') . 'task.todo';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        // composer.json
        $getFile = $this->options->get('templatePath') . '/project/pkmul/composer.json';
        $putFile = $this->options->get('projectPath') . 'composer.json';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        // bower with grunt / grunt-wiredep
        $getFile = $this->options->get('templatePath') . '/project/pkmul/bower.json';
        $putFile = $this->options->get('projectPath') . 'bower.json';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));
        $getFile = $this->options->get('templatePath') . '/project/pkmul/.bowerrc';
        $putFile = $this->options->get('projectPath') . '.bowerrc';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));
        $getFile = $this->options->get('templatePath') . '/project/pkmul/Gruntfile.js';
        $putFile = $this->options->get('projectPath') . 'Gruntfile.js';
        $this->generateFile($getFile, $putFile, $this->options->get('name'));

        return $this;
    }


    /**
     * Create .htrouter.php file
     *
     * @return $this
     */
    private function createHtrouterFile()
    {
        $getFile = $this->options->get('templatePath') . '/project/pkmul/.htrouter.php';
        $putFile = $this->options->get('projectPath') . '.htrouter.php';
        $this->generateFile($getFile, $putFile);

        return $this;
    }


    /**
     * Generate file $putFile from $getFile, replacing @@variableValues@@
     *
     * @param string $getFile From file
     * @param string $putFile To file
     * @param string $modname
     *
     * @return $this
     */
    private function generateFileWithModname($getFile, $putFile, $modname = '', $controllername = '')
    {
        if (false == file_exists($putFile)) {
            touch($putFile);
            $fh = fopen($putFile, "w+");

            $str = file_get_contents($getFile);

            $name = $this->options->get('name');
            $namespace = ucfirst( $this->options->get('name') );

            $str = preg_replace('/@@name@@/', $name, $str);
            $str = preg_replace('/@@namespace@@/', $namespace, $str);
            // 個別指定模組名稱
            $str = preg_replace('/@@modname@@/', $modname, $str);
            // 個別指定控制器名稱
            $str = preg_replace('/@@controllername@@/', $controllername, $str);

            if (sizeof($this->variableValues) > 0) {
                foreach ($this->variableValues as $variableValueKey => $variableValue) {
                    $variableValueKeyRegEx = '/@@'.preg_quote($variableValueKey, '/').'@@/';
                    $str = preg_replace($variableValueKeyRegEx, $variableValue, $str);
                }
            }

            fwrite($fh, $str);
            fclose($fh);
        }

        return $this;
    }

    /**
     * Build project
     *
     * @return bool
     */
    public function build()
    {
        $this
            ->buildDirectories()
            ->getVariableValues()
            ->createConfigFiles()
            ->createControllerFile()
            ->createViewFiles()
            ->createBootstrap()
            ->createHtaccessFiles()
            ->createPublicFiles()
            ->createPackageFile()
            ->createHtrouterFile();

        $this->options->contains('enableWebTools') && Tools::install($this->options->get('projectPath'));

        return true;
    }
}
